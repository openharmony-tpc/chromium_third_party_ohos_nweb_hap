# chromium_third_party_ohos_nweb_hap

## 简介
本仓为将OpenHarmony WebView内核编译生成的libneb_render.so、libweb_engine.so打包生成Nweb.hap的Hap包工程。

此仓不支持单独编译，开发者无需关注此仓，只需在门禁构建产物中获取Hap包即可。

## 相关仓
[chromium_src](https://gitee.com/openharmony-sig/chromium_src/blob/master)